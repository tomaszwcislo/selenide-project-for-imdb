The scenario is made for https://www.imdb.com/

The first test is conducted for search movie functionality.

Test 1 - searchMoviesTest

    1. open given URL 
    2. set value "Rocky" into search box
    3. assert that search result page contains "Rocky" string

test 2 - checkTopRatedMovies

    1. open given URL
    2. click menu button
    3. click Top Rated Movies button
    4. assert that displayed page contains header "Top rated movies"
    5. verify that first 10 movies have rating above 8

Report generating

    1. open a command prompt screen (in IDE we can use terminal directly)
    2. go to the project directory (if you use IDE's terminal it should be set as default)
    3. write the command:  allure serve allure-results