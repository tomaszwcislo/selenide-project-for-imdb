import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeSuite;

public class BaseTestClass {

    @BeforeSuite
    public void setUp() {
        Configuration.timeout = 5000;
    }
}
