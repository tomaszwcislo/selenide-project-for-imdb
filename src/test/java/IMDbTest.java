import com.codeborne.selenide.*;
import io.qameta.allure.*;
import jdk.jfr.Description;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.openqa.selenium.By.cssSelector;



public class IMDbTest extends BaseTestClass {

    @Test(priority = 1)
    @Epic("T01 - Functionality test")
    @Feature("Searching functionality")
    @Description("Test description: Check search movie functionality for existing movie")
    @Severity(SeverityLevel.CRITICAL)
    @Story("As a user I want searching movies by name")
    public void searchMovies() {

        open("https://www.imdb.com/");
        $(By.name("q")).setValue("Rocky").pressEnter();
        $(cssSelector("span.findSearchTerm")).shouldHave(Condition.exactText("\"Rocky\""));
    }

    @Test(priority = 2)
    @Epic("T100 - Functionality test")
    @Feature("Displaying functionality")
    @Description("Test description: Verify that first 10 movies have rating >8 in Top Rated Movies section")
    @Severity(SeverityLevel.NORMAL)
    @Story("As a user I want to see that first 10 movies have rating above 8")
    public void checkTopRatedMovies() {

        open("https://www.imdb.com/");
        $(cssSelector("div.ipc-button__text")).shouldBe(visible).click();
        $(cssSelector("a[href='/chart/top/?ref_=nv_mv_250']")).shouldBe(visible).click();
        $(cssSelector("h1")).shouldBe(visible).shouldHave(exactText("Top Rated Movies"));

        $$(cssSelector(".ratingColumn.imdbRating")).first(10).shouldHaveSize(10).texts()
                .forEach(strong->{assertThat(Double.parseDouble(strong.replace(",", "."))).isGreaterThan(8);});
        }
}




